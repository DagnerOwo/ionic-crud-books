import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Nosotros',
      url: '/nosotros',
      icon: 'logo-freebsd-devil'
    },
    {
      title: 'Books',
      url:'/books-list',
      icon: 'book'
    },
    {
      title: 'New Book',
      url:'/book-new',
      icon:'cloud-upload'
    },
    {
      title: 'Ayuda',
      url: '/ayuda',
      icon: 'md-help'
    }
    
    
  ];


  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
