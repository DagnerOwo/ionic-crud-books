import { Component, OnInit } from '@angular/core';
import { Book } from '../shared/book'
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BookService } from '../shared/book.service';
@Component({
  selector: 'app-book-new',
  templateUrl: './book-new.page.html',
  styleUrls: ['./book-new.page.scss'],
})
export class BookNewPage implements OnInit {
  errorMessage: string;
  bookForm: FormGroup;
  bookId:number;
  id: any;
  book: Book;
  constructor(private fb: FormBuilder,
    private activatedroute: ActivatedRoute,
    private router: Router,
    private bookService: BookService
    ) { }
  ngOnInit(): void {
    this.bookForm = this.fb.group({
      title: '',
      price: '',
      synopsis: '',
      isbn: '',
      image: ''
    });
    this.bookService.getMaxBookId().subscribe(
        data=>this.id = data
    );
    this.bookId = parseInt(this.id);
    
  }
  saveBook(){
    if (this.bookForm.valid){
      if(this.bookForm.dirty){
        this.book = this.bookForm.value;
        this.book.id = this.bookId;

        this.bookService.createBook(this.book).subscribe(
          ()=> this.onSaveComplete(),(error: any)=> this.errorMessage = <any>error
        );
      } else {
        this.onSaveComplete();
      }
    } else{
      this.errorMessage = 'please correct the validation errors.';
    }
  }
  onSaveComplete(): void {
    this.bookForm.reset();
    this.router.navigate(['/books-list']);
  }
}
