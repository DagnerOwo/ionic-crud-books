import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Book } from '../shared/book';
import { BookService } from '../shared/book.service';
@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.page.html',
  styleUrls: ['./book-edit.page.scss'],
})
export class BookEditPage implements OnInit {
  errorMessage: string;
  bookForm: FormGroup;

  bookId:number;
  book: Book;
  constructor(private fb: FormBuilder,
    private activatedroute:ActivatedRoute,
    private router: Router,
    private bookService: BookService
    ) { 
      this.book = {title:'',
    id: 0,
    price: 0,
    synopsis: '',
    image:'',
    isbn: ''
  };
    }

  ngOnInit(): void{
    this.bookForm = this.fb.group({
      title: '',
      price: 0,
      synopsis: '',
      isbn: '',
      image: ''
    });
    this.bookId = parseInt(this.activatedroute.snapshot.params['bookId']);
    this.getBook(this.bookId);
  }
  getBook(id: number): void{
    this.bookService.getBookById(id).subscribe(
      (book: Book) => this.displayBook(book),
      (error: any) => this.errorMessage = <any>error
    );
  }
  displayBook(book: Book): void{
    if(this.bookForm){
      this.bookForm.reset();
    }
    this.book = book;
    this.bookForm.patchValue({
      title: this.book.title,
      price: this.book.price,
      synopsis: this.book.synopsis,
      isbn: this.book.isbn,
      image: this.book.image
    });
  }
  deleteBook(): void{
    if (this.book.id ===0){
      this.onSaveComplete();
    } else {
      if (confirm(`Really delete the product: ${this.book.title}?`)){
        this.bookService.deleteBook(this.book.id).subscribe(
          ()=> this.onSaveComplete(),
          (error: any) => this.errorMessage = <any>error
        );
      }
    }
  }
  saveBook(): void{
    if (this.bookForm.valid){
      if (this.bookForm.dirty){
        this.book = this.bookForm.value;
        this.book.id = this.bookId;
        this.bookService.updateBook(this.book).subscribe(
          ()=>this.onSaveComplete(),
          (error: any)=>this.errorMessage = <any>error
        );
      } else{
        this.onSaveComplete();
      }
    }else{
      this.errorMessage = 'Please correct the validation errors.'
    }
  }
  onSaveComplete(): void{
    this.bookForm.reset();
    this.router.navigate(['/home']);
  }
}
