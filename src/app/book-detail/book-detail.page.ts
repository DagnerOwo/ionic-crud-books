import { Component, OnInit } from '@angular/core';
import { Book } from '../shared/book';
import { ActivatedRoute, Router } from '@angular/router';
import { BookService } from '../shared/book.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.page.html',
  styleUrls: ['./book-detail.page.scss'],
})
export class BookDetailPage implements OnInit {

  book: Book ;
  bookId: number;

  constructor(private activatedroute: ActivatedRoute, private router: Router, private productService: BookService) {
    this.book = {title:'',
    id: 0,
    price: 0,
    synopsis: '',
    image:'',
    isbn: ''
  };
  }
  ngOnInit() {
    this.bookId = parseInt(this.activatedroute.snapshot.params['bookId']);
    this.productService.getBookById(this.bookId).subscribe(
      (data: Book) => this.book = data
    );
  }
  goEdit():void{
    this.router.navigate(['/book-edit', this.bookId]);
  }
  onBack(): void {
    this.router.navigate(['/books-list']);
  }

}
