export interface Book {
    id: number;
    title: string;
    price: number;
    synopsis: string;
    isbn: string;
    image: '';
  }
  