import { InMemoryDbService } from 'angular-in-memory-web-api';

export class BookData implements InMemoryDbService {

  createDb() {
    let books = [
      {
        "id": 0,
        "title": "Libro 1",
        "price": 1000,
        "synopsis": "Contenido Contenido Contenido Contenido ContenidoContenidoContenidoContenidoContenido ContenidoContenidoContenidoContenido Contenido",
        "isbn": "5678UHG5678121DS",
        "image": "../assets/images/libro.jfif"
      },
      {
        "id": 1,
        "title": "Libro 2",
        "price": 1000,
        "synopsis": "Blablablabla BlablablablaBlablablablaBlablablablaBlablablablaBlablablablaBlablablablaBlablablabla",
        "isbn": "5678UHG5678121DS",
        "image": "../assets/images/libro.jfif"
      },
      {
        "id": 2,
        "title": "Viva la vida",
        "price": 1000,
        "synopsis": "Blablablabla BlablablablaBlablablablaBlablablabla",
        "isbn": "5678UHG5678121DS",
        "image": "../assets/images/libro.jfif"
      },
      {
        "id": 3,
        "title": "Viva la vida BlablablablaBlablablablaBlablablablaBlablablabla",
        "price": 1000,
        "synopsis": "Blablablabla",
        "isbn": "5678UHG5678121DS",
        "image": "../assets/images/libro.jfif"
      },
      {
        "id": 4,
        "title": "Viva la vida BlablablablaBlablablablaBlablablablaBlablablablas",
        "price": 1000,
        "synopsis": "Blablablabla",
        "isbn": "5678UHG5678121DS",
        "image": "../assets/images/libro.jfif"
      },
      {
        "id": 5,
        "title": "Viva la vida BlablablablaBlablablablaBlablablablaBlablablablaBlablablablaBlablablabla",
        "price": 1000,
        "synopsis": "Blablablabla",
        "isbn": "5678UHG5678121DS",
        "image": "../assets/images/libro.jfif"
      },
      {
        "id": 6,
        "title": "OWO",
        "price": 1000,
        "synopsis": "uwu uwu uwu uwu uwu uwu uwuuwuuwuuwu uwu uwu uwu uwu uwu uwu uwu uwu uwu uwu",
        "isbn": "5678UHG5678121DS",
        "image": "../assets/images/libro.jfif"
      },
      {
        "id": 7,
        "title": "Viva yo",
        "price": 1000,
        "synopsis": "owo owo owo owo owo owo owoowoowoowo owo owo owo owo owo",
        "isbn": "5678UHG5678121DS",
        "image": "../assets/images/libro.jfif"
      },
      {
        "id": 8,
        "title": "Viva la vida",
        "price": 1000,
        "synopsis": "Blablablabla",
        "isbn": "5678UHG5678121DS",
        "image": "../assets/images/libro.jfif"
      },
      
    ];
    return { books: books };
  }
}
